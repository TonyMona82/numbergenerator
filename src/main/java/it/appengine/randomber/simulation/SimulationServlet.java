package it.appengine.randomber.simulation;

import com.googlecode.objectify.ObjectifyService;
import it.appengine.randomber.simulation.dto.TaskDTO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

public class SimulationServlet extends HttpServlet {

    private static final Logger log = Logger.getLogger(SimulationServlet.class.getName());

    public static final int MIN_NUMBER;
    public static final int MAX_NUMBER;
    public static final int BATCH_NUMBER;
    public static final int TASK_NUMBER;

    static {
        MIN_NUMBER = 0;
        MAX_NUMBER = 50;
        BATCH_NUMBER = 100;
        TASK_NUMBER = 1000;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //start
        long lStartTime = System.nanoTime();

        log.info("Starting Simulation");
        deleteSimulation();
        saveSimulation();
        getDifferentNumber();
        getPercentageRepeatedValue();
        //end
        long lEndTime = System.nanoTime();
        //time elapsed
        long output = lEndTime - lStartTime;
        log.info("Ending Simulation in ms " + output / 1000000);

    }

    private void saveSimulation() {
        ArrayList tasks = new ArrayList<TaskDTO>();

        //crete 100 simulation
        for (int i = 0; i < BATCH_NUMBER; i++) {
            ArrayList numbers = new ArrayList<Integer> ();
            //create 1000 task
            for (int j = 0; j < TASK_NUMBER; j++) {
                // nextInt is normally exclusive of the top value,
                // so add 1 to make it inclusive
                int randomNum = ThreadLocalRandom.current().nextInt(MIN_NUMBER, MAX_NUMBER + 1);
                numbers.add(randomNum);
            }
            tasks.add(new TaskDTO(i,numbers));
        }

        //save tasks to datastore
        ObjectifyService.ofy().save().entities(tasks).now();
        log.info("Entity saved --> " + tasks.size());

    }

    private void deleteSimulation() {
        //get tasks from datastore
        List<TaskDTO> list = ObjectifyService.ofy().load().type(TaskDTO.class).list();
        for (TaskDTO task : list) {
            //System.out.println(task.order);
            ObjectifyService.ofy().delete().entity(task);
        }
        log.info("Entity deleted --> " + list.size());

    }

    private void getSimulation() {
        //get tasks from datastore
        List<TaskDTO> list = ObjectifyService.ofy().load().type(TaskDTO.class).list();
        for (TaskDTO task : list) {
            System.out.println(task.order);
        }
        log.info("Entity founded --> " + list.size());

    }

    private void getDifferentNumber() {
        //get tasks from datastore
        List<TaskDTO> list = ObjectifyService.ofy().load().type(TaskDTO.class).list();
        Set<Integer> numberSet = new HashSet<Integer>();
        for (TaskDTO task : list) {
            numberSet.addAll(task.randomNumbers);
        }
        log.info("Set --> " + numberSet);
        log.info("Set size --> " + numberSet.size());

    }

    private void getPercentageRepeatedValue() {
        //get tasks from datastore
        List<TaskDTO> list = ObjectifyService.ofy().load().type(TaskDTO.class).list();
        List<Integer> listTotal = new ArrayList<Integer>();
        for (TaskDTO task : list) {
            listTotal.addAll(task.randomNumbers);
        }
        log.info("Total numbers --> " + listTotal.size());

        Map<Integer, Integer> nameAndCount = new HashMap();
        // build hash table with count
        for (Integer number : listTotal) {
            Integer count = nameAndCount.get(number);
            if (count == null) {
                nameAndCount.put(number, 1);
            }
            else {
                nameAndCount.put(number, ++count);
            }
        }
        Integer maxValue = 0;
        Integer maxNumber = 0;
        // Print duplicate elements from array in Java
        Set<Map.Entry<Integer, Integer>> entrySet = nameAndCount.entrySet();
        for (Map.Entry<Integer, Integer> entry : entrySet) {
            if (entry.getValue() > 1) {
                if (entry.getValue() > maxValue) {
                    maxValue = entry.getValue();
                    maxNumber = entry.getKey();
                }
                log.info("Duplicate element from array : " + entry.getKey() + "x " +entry.getValue());
            }
        }
        log.info("Number most duplicated : " + maxNumber + " duplicated for time " +maxValue);
    }
}