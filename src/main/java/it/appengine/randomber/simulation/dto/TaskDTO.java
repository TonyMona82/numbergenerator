package it.appengine.randomber.simulation.dto;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.util.ArrayList;

@Entity
public class TaskDTO {

    @Id
    public Long id;

    public Integer order;
    public ArrayList<Integer> randomNumbers;

    public TaskDTO(){}

    public TaskDTO(Integer ord, ArrayList<Integer> numbers){
        order = ord;
        randomNumbers = numbers;
    }
}
