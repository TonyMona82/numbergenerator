package it.appengine.randomber.auth;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UsersServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        System.out.println(user);

        String thisUrl = req.getRequestURI();
        resp.setContentType("text/html");
 /*
        if (user != null && user.getEmail().contains("tony.monacizzo@gmail.com")){
            resp.getWriter().println("<p>Hello, "
                    + req.getUserPrincipal().getName()
                    + "!  You can <a href=\""
                    + userService.createLogoutURL(thisUrl)
                    + "\">sign out</a>.</p>");
        }
        else {
            resp.getWriter().println("<p>Please <a href=\""
                    + userService.createLoginURL(thisUrl)
                    + "\">sign in</a>.</p>");
        }
*/
        if (req.getUserPrincipal() != null) {
            resp.getWriter().println("<p>Hello, "
                    + req.getUserPrincipal().getName()
                    + "!  You can <a href=\""
                    + userService.createLogoutURL(thisUrl)
                    + "\">sign out</a>.</p>");
        } else {
            resp.getWriter().println("<p>Please <a href=\""
                    + userService.createLoginURL(thisUrl)
                    + "\">sign in</a>.</p>");
        }

    }

}
