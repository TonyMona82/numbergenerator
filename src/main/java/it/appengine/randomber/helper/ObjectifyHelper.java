package it.appengine.randomber.helper;

import com.googlecode.objectify.ObjectifyService;
import it.appengine.randomber.simulation.dto.TaskDTO;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ObjectifyHelper implements ServletContextListener {
    public void contextInitialized(ServletContextEvent event) {
        // This will be invoked as part of a warmup request, or the first user request if no warmup
        // request.

        ObjectifyService.register(TaskDTO.class);
    }

    public void contextDestroyed(ServletContextEvent event) {
        // App Engine does not currently invoke this method.
    }
}
